<?php
$page = "otzyvy";
include "start.php";


if (isset($_POST['submit'])) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$msg = $_POST['msg'];

	$add_otzyv = addOtzyv($name, $email, $msg);
	if ($add_otzyv) {
		header("Location: otzyvy.php");
		exit();
	}
}

?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$data_pages['title'];?></title>
	<meta name="description" content="<?=$data_pages['meta_d'];?>">
	<meta name="keywords" content="<?=$data_pages['meta_k'];?>">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>

	<div id="container">
		<div id="header">
			<div class="logo">
				<a href="/"><img src="img/logo.png" alt=""></a>
			</div>
			<div class="info-comp">
				<div class="name-comp">Брянский молочный комбинат</div>
				<div class="contact-comp">
					г. Брянск, ул. 50-й Армии, 2Б
				</div>
			</div>
		</div>

		<div id="menu">
			<ul class="menu">
				<li><a href="/index.php">Главная</a></li>
				<li><a href="/productions.php">Продукция</a></li>
				<li><a href="/o_nas.php">О нас</a></li>
				<li><a href="/kollektive.php">Наш коллектив</a></li>
				<li><a href="/otzyvy.php">Отзывы</a></li>
				<li><a href="/news.php">Новости</a></li>
				<li><a href="/contacts.php">Контакты</a></li>
			</ul>
		</div>

		<div id="content">
			<h1><?=$data_pages['h1'];?></h1>
			<div class="text">
				<?=$data_pages['text'];?>
			</div>

			<div class="form-otzyv">
				<div class="head-form">Оставьте свой отзыв</div>
				<form action="" method="post">
					<div class="form-group">
						<label for="name">Имя</label>
						<input type="text" id="name" name="name" placeholder="Имя">
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="text" id="email" name="email" placeholder="E-mail">
					</div>
					<div class="form-group">
						<label for="msg">Ваше сообщение</label>
						<textarea name="msg" id="msg" cols="30" rows="10"></textarea>
					</div>
					<div class="form-group">
						<input type="submit" name="submit" value="Отправить">
					</div>
				</form>
			</div>

			<div class="otzyvy">
				<? foreach($data_otzyvy as $otzyv):?>
				<div class="post">
					<div class="post-info">
						<div class="user"><?=$otzyv['name'];?></div>
						<div class="date"><?=$otzyv['date'];?></div>
					</div>
					<div class="text-post">
						<?=$otzyv['msg'];?>
					</div>
				</div>
				<? endforeach;?>
			</div>
		</div>

		<div id="clear"></div>

		<div id="footer">
			
			<div class="left-footer">
				2016 &copy; ОАО "БМК"
			</div>
			<div class="right-footer">
				г. Брянск, ул. 50-й Армии, 2Б<br>
				e-mail: bmk@ipcity.ru
			</div>
		</div>
	</div>

</body>
</html>