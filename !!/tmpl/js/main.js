$(document).ready(function(){
	$(".slider>#owl-demo").owlCarousel({
		autoPlay: 3000,
		navigation : true, 
		slideSpeed : 300,
		paginationSpeed : 400,
		singleItem:true
	});

	var owl = $(".hits>#owl-demo");
	owl.owlCarousel({
      items : 6,
      itemsDesktop : [1000,5],
      itemsDesktopSmall : [900,3],
      itemsTablet: [600,2],
      itemsMobile : false
  });
	owl.trigger('owl.play', 1000);
	$(".next").click(function(){
		owl.trigger('owl.next');
	})
	$(".prev").click(function(){
		owl.trigger('owl.prev');
	})
});