<?php
$page = "main";
include "start.php";

?>
<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="UTF-8">
	<title><?=$data_pages['title'];?></title>
	<meta name="description" content="<?=$data_pages['meta_d'];?>">
	<meta name="keywords" content="<?=$data_pages['meta_k'];?>">
	<link rel="stylesheet" href="/css/style.css">
</head>
<body>

	<div id="container">
		<div id="header">
			<div class="logo">
				<a href="/"><img src="img/logo.png" alt=""></a>
			</div>
			<div class="info-comp">
				<div class="name-comp">Брянский молочный комбинат</div>
				<div class="contact-comp">
					г. Брянск, ул. 50-й Армии, 2Б
				</div>
			</div>
		</div>

		<div id="menu">
			<ul class="menu">
				<li><a href="/index.php">Главная</a></li>
				<li><a href="/productions.php">Продукция</a></li>
				<li><a href="/o_nas.php">О нас</a></li>
				<li><a href="/kollektive.php">Наш коллектив</a></li>
				<li><a href="/otzyvy.php">Отзывы</a></li>
				<li><a href="/news.php">Новости</a></li>
				<li><a href="/contacts.php">Контакты</a></li>
			</ul>
		</div>

		<div id="content">
			<h1><?=$data_pages['h1'];?></h1>
			<div class="text">
				<?=$data_pages['text'];?>
			</div>

			<div class="hits">
				<div class="head-hits">Хиты продаж</div>
				<ul class="hits">
					<li><img src="img/hits/34.jpg" alt=""></li>
					<li><img src="img/hits/36.jpg" alt=""></li>
					<li><img src="img/hits/41.jpg" alt=""></li>
					<li><img src="img/hits/45.jpg" alt=""></li>
				</ul>
			</div>
		</div>

		<div id="clear"></div>

		<div id="footer">
			
			<div class="left-footer">
				2016 &copy; ОАО "БМК"
			</div>
			<div class="right-footer">
				г. Брянск, ул. 50-й Армии, 2Б<br>
				e-mail: bmk@ipcity.ru
			</div>
		</div>
	</div>

</body>
</html>